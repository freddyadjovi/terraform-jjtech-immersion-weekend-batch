



#### Dynamic blocks for security groups
variable "sg_ports" {
  type        = list(any)
  description = "List of ingress ports to open"
  default     = ["443", "80", "9000", "9300", "8300", "3306", "25"]

}


resource "aws_security_group" "name" {
  name = "jjtech-demo"
  dynamic "ingress" {
    for_each = var.sg_ports
    content {
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow traffic fro ssh, https etc..."
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"

    }

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# resource "aws_security_group" "dynamic_sg" {
#   name = "dynamic_sg"

#   dynamic "ingress" {
#     for_each = var.sg_ports
#     iterator = ports
#     content {
#       from_port = ports.value
#       to_port = ports.value
#       protocol = "tcp"
#       cidr_blocks = [ "0.0.0.0/0" ]
#     }
#   }
#    egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# # # ##########################################################################


